import os
import queue
import time
import urllib
from threading import Thread

import requests


# https://api.unsplash.com/search/collections?page=1&query=office  (this works too and is aggregate?)

def scrapeimages(q, keyword="", num=35):
    tasker = q
    count = 0
    page = 1
    found = []
    totalmaxfinds = 0
    while (count < num):
        url = "http://pixabay.com/api/?key=5827797-d423aae847a5bce73e8bc6739&per_page={}&page={}&q={}&pretty=1".format(
            num, page, str(keyword))
        # url.format(num, page, str(keyword))
        print(url)
        time.sleep(.1)
        pass_json = False
        while (not pass_json):
            try:
                jsondata = requests.get(url.format(q, page, keyword)).json()
                totalmaxfinds = (str(jsondata).count('previewURL'))
                pass_json = True
            except:
                time.sleep(1)
                print("Error")

        if ((int(count) + 2) >= int(totalmaxfinds)):
            print("Max Found")
            return ''

        print("Found on JSON: ", (str(jsondata).count('previewURL')))

        for i in range((str(jsondata).count('previewURL'))):
            if (count >= num): break
            tmp = (jsondata['hits'][i]['previewURL'])
            if (not found.__contains__(tmp)):
                count += 1
                # print("Downloading image: ",i + ((page-1) * 200))
                found.append(tmp)
                dir = (str(os.getcwd()) + "/images/" + str(keyword))
                try:
                    os.mkdir(dir)
                except:
                    pass
                # same_threaded_job( urllib.request.urlretrieve, [str(tmp),keyword + "/" + str(i) + ".jpg"] )

                tasker.add_task(urllib.request.urlretrieve, str(tmp),
                                str(dir + '//' + str(i + ((page - 1) * 200)) + ".jpg"))
                # urllib.request.urlretrieve(str(tmp), keyword + "/" + str(i) + ".jpg")
        if (len(found) > 199):
            print("PAGE INCREASED")
            page += 1


class TaskQueue(queue.Queue):
    tasker = None

    def __init__(*args):
        # print (args)
        tasker = queue.Queue.__init__(args[0])

        args[0].num_workers = args[1]
        args[0].start_workers()

    def add_task(self, task, *args, **kwargs):
        args = args or ()
        # print(args)
        kwargs = kwargs or {}
        self.put((task, args, kwargs))

    def start_workers(self):
        for i in range(self.num_workers):
            t = Thread(target=self.worker)
            t.daemon = True
            t.start()

    def worker(self):
        while True:
            # try:
            item, args, kwargs = self.get()
            # print(item,args,kwargs)
            item(*args, **kwargs)
            arg1 = str(args[1])
            # print(arg1)
            print("Downloading " + str((arg1).split("//")[-1:]))
            self.task_done()

            # except Exception as e :
            #     print(e)


def start_downloads():
    q = TaskQueue(4)
    scrapeimages(q, "China", num=120)
    q.join()
    print("Finished?")


start_downloads()
